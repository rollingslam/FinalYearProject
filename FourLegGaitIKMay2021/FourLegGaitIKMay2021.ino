#include <Wire.h>
#include <math.h>
#include <Adafruit_PWMServoDriver.h>
#include <FlexiTimer2.h>

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

#define USMIN 600;
#define USMAX 2400;
#define SERVO_FREQ 50;

/* Size of the robot ---------------------------------------------------------*/
const float length_a = 70; //55;
const float length_b = 37; //77.5;
const float length_c = 70; //27.5;
const float length_side = 70; //71;
const float z_absolute = -28;
/* Constants for movement ----------------------------------------------------*/
const float z_default = -50, z_up = -30, z_boot = z_absolute;
const float x_default = 62, x_offset = 0;
const float y_start = 0, y_step = 40;
const float y_default = x_default;
/* variables for movement ----------------------------------------------------*/
volatile float site_now[4][3];    //real-time coordinates of the end of each leg
volatile float site_expect[4][3]; //expected coordinates of the end of each leg
float temp_speed[4][3];   //each axis' speed, needs to be recalculated before each movement
float move_speed;     //movement speed
float speed_multiple = 1; //movement speed multiple
const float spot_turn_speed = 4;
const float leg_move_speed = 4; //8;
const float body_move_speed = 1.5; //3;
const float stand_seat_speed = 1;
volatile int rest_counter;      //+1/0.02s, for automatic rest
//functions' parameter
const float KEEP = 255;
//define PI for calculation
const float pi = 3.1415926;
/* Constants for turn --------------------------------------------------------*/
//temp length
const float temp_a = sqrt(pow(2 * x_default + length_side, 2) + pow(y_step, 2));
const float temp_b = 2 * (y_start + y_step) + length_side;
const float temp_c = sqrt(pow(2 * x_default + length_side, 2) + pow(2 * y_start + y_step + length_side, 2));
const float temp_alpha = acos((pow(temp_a, 2) + pow(temp_b, 2) - pow(temp_c, 2)) / 2 / temp_a / temp_b);
//site for turn
const float turn_x1 = (temp_a - length_side) / 2;
const float turn_y1 = y_start + y_step / 2;
const float turn_x0 = turn_x1 - temp_b * cos(temp_alpha);
const float turn_y0 = temp_b * sin(temp_alpha) - turn_y1 - length_side;
/* ---------------------------------------------------------------------------*/

int servocurrent[4][3];

void setup() {
  // put your setup code here, to run once:
  pwm.reset();
  pwm.begin();
  pwm.setOscillatorFrequency(27000000);
  pwm.setPWMFreq(50);
  pinMode(LED_BUILTIN, OUTPUT);

  yield();

  //robotLegSetup();

    //start serial for debug
  Serial.begin(9600);
  Serial.println("Robot starts initialization");
  //initialize default parameter
  set_site(0, x_default - x_offset, y_start + y_step, z_boot);
  set_site(1, x_default - x_offset, y_start + y_step, z_boot);
  set_site(2, x_default + x_offset, y_start, z_boot);
  set_site(3, x_default + x_offset, y_start, z_boot);
  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      site_now[i][j] = site_expect[i][j];
    }
  }
  //start servo service
  FlexiTimer2::set(20, servo_service);
  FlexiTimer2::start();
  Serial.println("Servo service started");
  //initialize servos
  Serial.println("Servos initialized");
  Serial.println("Robot initialization Complete");

}

void loop() {
  step_forward(2);
  delay(5000);
}

/*
  - go forward
  - blocking function
  - parameter step steps wanted to go
   ---------------------------------------------------------------------------*/
void step_forward(unsigned int step)
{
  Serial.println("step_forward");
  move_speed = leg_move_speed;
  while (step-- > 0)
  {
    if (site_now[2][1] == y_start)
    {
      //leg 2&1 move
      Serial.println("leg 2 move");
      set_site(2, x_default + x_offset, y_start, z_up);
      wait_all_reach();
      set_site(2, x_default + x_offset, y_start + 2 * y_step, z_up);
      wait_all_reach();
      set_site(2, x_default + x_offset, y_start + 2 * y_step, z_default);
      wait_all_reach();

      move_speed = body_move_speed;

      Serial.println("leg 0 1 2 3 move");
      set_site(0, x_default + x_offset, y_start, z_default);
      set_site(1, x_default + x_offset, y_start + 2 * y_step, z_default);
      set_site(2, x_default - x_offset, y_start + y_step, z_default);
      set_site(3, x_default - x_offset, y_start + y_step, z_default);
      wait_all_reach();

      move_speed = leg_move_speed;

      Serial.println("leg 1 move");
      set_site(1, x_default + x_offset, y_start + 2 * y_step, z_up);
      wait_all_reach();
      set_site(1, x_default + x_offset, y_start, z_up);
      wait_all_reach();
      set_site(1, x_default + x_offset, y_start, z_default);
      wait_all_reach();
    }
    else
    {
      //leg 0&3 move
      Serial.println("leg 0 move");
      set_site(0, x_default + x_offset, y_start, z_up);
      wait_all_reach();
      set_site(0, x_default + x_offset, y_start + 2 * y_step, z_up);
      wait_all_reach();
      set_site(0, x_default + x_offset, y_start + 2 * y_step, z_default);
      wait_all_reach();

      move_speed = body_move_speed;

      Serial.println("leg 0 1 2 3 move (else)");
      set_site(0, x_default - x_offset, y_start + y_step, z_default);
      set_site(1, x_default - x_offset, y_start + y_step, z_default);
      set_site(2, x_default + x_offset, y_start, z_default);
      set_site(3, x_default + x_offset, y_start + 2 * y_step, z_default);
      wait_all_reach();

      move_speed = leg_move_speed;

      Serial.println("leg 3 move");
      set_site(3, x_default + x_offset, y_start + 2 * y_step, z_up);
      wait_all_reach();
      set_site(3, x_default + x_offset, y_start, z_up);
      wait_all_reach();
      set_site(3, x_default + x_offset, y_start, z_default);
      wait_all_reach();
    }
  }
}

/*
  - microservos service /timer interrupt function/50Hz
  - when set site expected,this function move the end point to it in a straight line
  - temp_speed[4][3] should be set before set expect site,it make sure the end point
   move in a straight line,and decide move speed.
   ---------------------------------------------------------------------------*/
void servo_service(void)
{

  sei();
  static float alpha, beta, gamma;

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      if (abs(site_now[i][j] - site_expect[i][j]) >= abs(temp_speed[i][j]))
        site_now[i][j] += temp_speed[i][j];
      else
        site_now[i][j] = site_expect[i][j];
    }

    cartesian_to_polar(alpha, beta, gamma, site_now[i][0], site_now[i][1], site_now[i][2]);
    polar_to_servo(i, alpha, beta, gamma);
  }

  rest_counter++;
}

/*
  - set one of end points' expect site
  - this founction will set temp_speed[4][3] at same time
  - non - blocking function
   ---------------------------------------------------------------------------*/
void set_site(int leg, float x, float y, float z)
{
  float length_x = 0, length_y = 0, length_z = 0;

  if (x != KEEP)
    length_x = x - site_now[leg][0];
  if (y != KEEP)
    length_y = y - site_now[leg][1];
  if (z != KEEP)
    length_z = z - site_now[leg][2];

  float length = sqrt(pow(length_x, 2) + pow(length_y, 2) + pow(length_z, 2));

  temp_speed[leg][0] = length_x / length * move_speed * speed_multiple;
  temp_speed[leg][1] = length_y / length * move_speed * speed_multiple;
  temp_speed[leg][2] = length_z / length * move_speed * speed_multiple;

  if (x != KEEP)
    site_expect[leg][0] = x;
  if (y != KEEP)
    site_expect[leg][1] = y;
  if (z != KEEP)
    site_expect[leg][2] = z;
}

/*
  - wait one of end points move to expect site
  - blocking function
   ---------------------------------------------------------------------------*/
void wait_reach(int leg)
{
  while (1)
    if (site_now[leg][0] == site_expect[leg][0])
      if (site_now[leg][1] == site_expect[leg][1])
        if (site_now[leg][2] == site_expect[leg][2])
          break;
}

/*
  - wait all of end points move to expect site
  - blocking function
   ---------------------------------------------------------------------------*/
void wait_all_reach(void)
{
  for (int i = 0; i < 4; i++)
    wait_reach(i);
}

/*
  - trans site from cartesian to polar
  - mathematical model 2/2
   ---------------------------------------------------------------------------*/
void cartesian_to_polar(volatile float &alpha, volatile float &beta, volatile float &gamma, volatile float x, volatile float y, volatile float z)
{
  //calculate w-z degree
  float v, w;
  w = (x >= 0 ? 1 : -1) * (sqrt(pow(x, 2) + pow(y, 2)));
  v = w - length_c;
  //alpha: knee   beta: lift   gamma:swing
  alpha = atan2(z, v) + acos((pow(length_a, 2) - pow(length_b, 2) + pow(v, 2) + pow(z, 2)) / 2 / length_a / sqrt(pow(v, 2) + pow(z, 2)));
  beta = acos((pow(length_a, 2) + pow(length_b, 2) - pow(v, 2) - pow(z, 2)) / 2 / length_a / length_b);
  //calculate x-y-z degree
  gamma = (w >= 0) ? atan2(y, x) : atan2(-y, -x);
  //trans degree pi->180
  alpha = alpha / pi * 180;
  beta = beta / pi * 180;
  gamma = gamma / pi * 180;
}

/*
  - trans site from polar to microservos
  - mathematical model map to fact
  - the errors saved in eeprom will be add
   ---------------------------------------------------------------------------*/
void polar_to_servo(int leg, float alpha, float beta, float gamma)
{
  if (leg == 0)
  {
    alpha = 90 - alpha;
    beta = beta;
    gamma += 90;
  }
  else if (leg == 1)
  {
    alpha += 90;
    beta = 180 - beta;
    gamma = 90 - gamma;
  }
  else if (leg == 2)
  {
    alpha += 90;
    beta = 180 - beta;
    gamma = 90 - gamma;
  }
  else if (leg == 3)
  {
    alpha = 90 - alpha;
    beta = beta;
    gamma += 90;
  }

  //Serial.println("Leg" + String(leg) + "angle" + String(alpha) + " " + String(beta) + " " + String(gamma) );

  if (leg = 0){
    servoDeg("a1", alpha);
    servoDeg("a2", beta);
    servoDeg("a3", gamma);
  }
  else if (leg = 1){
    servoDeg("b1", alpha);
    servoDeg("b2", beta);
    servoDeg("b3", gamma);
  }
  else if (leg = 2){
    servoDeg("c1", alpha);
    servoDeg("c2", beta);
    servoDeg("c3", gamma); 
  }
  else if (leg = 3){
    servoDeg("d1", alpha);
    servoDeg("d2", beta);
    servoDeg("d3", gamma);
  }
}

void servoDeg(String servoID, int deg){
  int leg, axis, servoNum;

  if (servoID=="a1"){
    servoNum = 0;
    leg = 0;
    axis = 0;
  }
  if (servoID=="a2"){
    servoNum = 1;
    leg = 0;
    axis = 1;
  }
  if (servoID=="a3"){
    servoNum = 2;
    leg = 0;
    axis = 2;
  }
  //---------------------
  if (servoID=="b1"){
    servoNum = 4;
    leg = 1;
    axis = 0;
  }
  if (servoID=="b2"){
    servoNum = 5;
    leg = 1;
    axis = 1;
  }
  if (servoID=="b3"){
    servoNum = 6;
    leg = 1;
    axis = 2;
  }
  //---------------------
  if (servoID=="c1"){
    servoNum = 8;
    leg = 2;
    axis = 0;
  }
  if (servoID=="c2"){
    servoNum = 9;
    leg = 2;
    axis = 1;
  }
  if (servoID=="c3"){
    servoNum = 10;
    leg = 2;
    axis = 2;
  }
  //---------------------
  if (servoID=="d1"){
    servoNum = 12;
    leg = 3;
    axis = 0;
  }
  if (servoID=="d2"){
    servoNum = 13;
    leg = 3;
    axis = 1;
  }
  if (servoID=="d3"){
    servoNum = 14;
    leg = 3;
    axis = 2;
  }

  pwm.setPWM(servoNum, 0, angleToPulse(deg, leg, axis));
  servocurrent[leg][axis] = deg;
}

int angleToPulse(int ang, int leg, int axis){
  long servoMIN[4][3]={
    {100, 125, 115},
    {115, 125, 115},
    {115, 105, 125},
    {130, 95, 90}
  };

  long servoMAX[4][3]={
    {500, 520, 505},
    {510, 515, 495},
    {485, 490, 500},
    {510, 480, 485}
  };

  long servoAngMIN[4][3]{
    {45, 50, 0},
    {45, 45, 50},
    {45, 50, 0},
    {45, 45, 50},
  };

  long servoAngMAX[4][3]{
    {135, 135, 130},
    {135, 130, 180},
    {135, 135, 130},
    {135, 130, 180},
  };

  if(ang < servoAngMIN[leg][axis]){
    ang = servoAngMIN[leg][axis];
  }else if (ang > servoAngMAX[leg][axis]) {
    ang = servoAngMAX[leg][axis];
  }

  // map angle of 0 to 180 to Servo min and Servo max 
  int pulse = map(ang, 0, 180, servoMIN[leg][axis], servoMAX[leg][axis]); 
  //Serial.print("Angle: ");Serial.print(ang);
  //Serial.print(" pulse: ");Serial.println(pulse);
  return pulse;
}

void robotSetup(){
  servocurrent[0][0]=90;
  servoDeg("a1", 90);
  servocurrent[0][1]=50;
  servoDeg("a2", 50);
  servocurrent[0][2]=45;
  servoDeg("a3", 45);

  servocurrent[1][0]=90;
  servoDeg("b1", 90);
  servocurrent[1][1]=130;
  servoDeg("b2", 130);
  servocurrent[1][2]=135;
  servoDeg("b3", 135);

  servocurrent[2][0]=90;
  servoDeg("c1", 90);
  servocurrent[2][1]=50;
  servoDeg("c2", 50);
  servocurrent[2][2]=45;
  servoDeg("c3", 45);

  servocurrent[3][0]=90;
  servoDeg("d1", 90);
  servocurrent[3][1]=130;
  servoDeg("d2", 130);
  servocurrent[3][2]=135;
  servoDeg("d3", 135);
}
